import React, { Fragment } from 'react';

// Components
import SearchBar from '../search/SearchBar';
import Users from '../users/Users';

const Home = () => (
    <Fragment>
      <SearchBar />
      <Users />
    </Fragment>
  );

export default Home
