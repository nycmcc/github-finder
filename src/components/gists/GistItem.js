import React from 'react';
import PropTypes from 'prop-types';

/*********************************************************************
 * Gists Component
 * 1) Created a functional component
 * 2) Because the Github Gist API is based on "array like object",
 *    you could use two different methods that returns the object's
 *    enumerable property values:
 *    - Object.values()
 *    - Object.keys()
***********************************************************************/
const GistItem = ({ gist }) => {
  return (
    <div className="card">
      <h3>
        <a href={gist.html_url}
          // Note: "target="blank"" means opening a new page
          target="blank"
        >
          {/* 
              Object.keys - returns an array of a given object's 
              own enumerable property "names"
                - in the same order as we get with a normal "for" loop, which
                  loops through a block of code a number of times
                  https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys
          */}
          {/* {Object.keys(gist.files)[0]} */}

          {/* 
            Object.values - returns an array of a given object's 
            own enumerable property values
              - in the same order as that provided by a "for...in" loop,
                which loops through the properties of an object.
                - The "difference" being that a for-in loop enumerates 
                  properties in the prototype chain as well
                  https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_objects/Object/values
          */}
          {Object.values(gist.files)[0].filename}
          {/* "filename" is files' first property value */}
        </a>
      </h3>
    </div>
  )
};

GistItem.propTypes = {
  gist: PropTypes.object.isRequired,
};

export default GistItem;
