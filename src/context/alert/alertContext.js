import { createContext } from 'react';

// Alert Context
const alertContext = createContext();

export default alertContext;