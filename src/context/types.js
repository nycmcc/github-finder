/************************************************************************************
 * Implementing Context API in Hooks
 * 1) types are "variables of strings" that you can use that you can call to change 
 *    the "state" within the reducer.
************************************************************************************/

// These are the "types" for this application
export const SEARCH_USERS = 'SEARCH_USERS';
export const GET_USER = 'GET_USER';
export const CLEAR_USERS = 'CLEAR_USERS';
export const GET_REPOS = 'GET_REPOS';
export const GET_GISTS = 'GET_GISTS';
export const SET_LOADING = 'SET_LOADING';
export const SET_ALERT = 'SET_ALERT';
export const REMOVE_ALERT = 'REMOVE_ALERT';