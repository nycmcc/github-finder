import { createContext } from 'react';

// Github Context
const githubContext = createContext();

export default githubContext;