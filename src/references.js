import React from 'react';
import './App.css';

import { Component } from 'react'

// export default class App extends Component {
//   render() {
//     return React.createElement('div', { className: 'App' }, React.createElement('h1', null, 'hello from react')
//       // vanilla js: anytime you want to render out any element -> use this
//       // null - no attributes
      // for attribute
      // <label for="name">Name</label> <- Get a warning: Invalid DOM property `for`. Did you mean `htmlFor`? 
       // change it to: <label htmlFor="name">Name</label>
//     );
//   }
// }

export default class App extends Component {
  // create a method as part of my class
  foo = () => 'Bars'

  render() {
    // variable
    const name = "john doe";

    // create a foo method
    const foo = () => 'Bar'

    // conditionals (e.g. fetching data)
    const loading = false; // boolean; if true - shows "loading" page

    // Single conditional
    // if (loading) {
    //  return <h4>Loading...</h4> 
    // }

    const showName = true; // if "false" shows "hello" and if true shows "hello jon doe"

    return (
      <div className='App'>
        <h1>Hello</h1>

        {/* <h1>{name}</h1> variable is shown */}

        <h1>{name.toUpperCase()}</h1> {/* adding a method */}

        <h1>{1 + 1}</h1>

        <h1>{foo()}</h1> {/* foo method in render */}

        <h1>{this.foo()}</h1> {/* foo method in class */}

        {/* when loading is "true", could add other stuff and still see
          loading page with the added stuff 
        */}

        {loading ? <h4>Loading...</h4> : <h1>hello {showName ? name : null}</h1>}{/* ternary operator - conditional: showing either loading or (:) hello name  */}
        {/* option 1: <h1>hello {showName ? name : null}</h1> and option 2: <h1>hello {showName && name}</h1>*/}
      </div>
    );
  }
}


// Notes:
// 1) When it comes to "props", you can pass "props" from one component to another component (see App component) 
// or you can use "defaultProps"(see Navbar component)