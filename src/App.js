import React from 'react';

// Css Stylesheet
import './App.css';

// Layout Components
import Navbar from './components/layout/Navbar';
import Alert from './components/layout/Alert';
// User Components
import User from './components/users/User';
// Pages Components
import About from './components/pages/About';
import Home from './components/pages/Home';
import NotFound from './components/pages/NotFound';

// Context APIs States
import GithubState from './context/github/GithubState';
import AlertState from './context/alert/AlertState';

// Third Party Libraries
// React-Router-Dom
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

const App = () => {
  return (
    <GithubState>
      <AlertState>
        <Router>
          <div className='App'>
            <Navbar />
            <div className='container'>
              <Alert />
              <Switch>
                <Route exact path='/' component={Home} />
                <Route exact path='/about' component={About} />
                <Route exact path='/user/:login' render={props => (
                    <User {...props} />
                  )}
                />
                <Route component={NotFound} />
              </Switch>
            </div>
          </div>
        </Router>
      </AlertState>
    </GithubState>
  );
};

export default App;
